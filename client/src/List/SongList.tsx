import * as React from "react";
import {ListItem, ListItemText, ListItemSecondaryAction, IconButton, List} from "@material-ui/core";
import styled from "styled-components";

interface IProps {
    removeDisabled: boolean;
}

interface IState {
    songs: string[];
}

class SongList extends React.Component<IProps, IState> {
    constructor(props: IProps) {
        super(props);

        this.state = {
            songs: []
        };
    }

    public async componentWillMount() {

        setTimeout(() => this.setState({
            songs: [
                "ahoj1",
                "ahoj2",
                "ahoj3",
                "ahoj4",
                "ahoj4",
                "ahoj4",
                "ahoj4",
                "ahoj4",
                "ahoj4",
                "ahoj4",
                "ahoj4",
                "ahoj4",
                "ahoj4",
                "ahoj4",
                "ahoj4",
                "ahoj4",
                "ahoj4",
                "ahoj4",
                "ahoj4",
                "ahoj4",
                "ahoj4",
                "ahoj4",
                "ahoj4",
                "ahoj4",
                "ahoj4",
                "ahoj4",
                "ahoj4",
            ]
        }));
        // const songs = await fetch("/songs").then(data => data.json()).then(json => json.songs);
        /*this.setState({
            songs
        })*/
    }


    public render() {
        const StyledList = styled(List)`
          overflow: auto;
        `;
        return (
            <StyledList>
                {this.state.songs.map(value => (
                    <ListItem key={value} role={undefined} dense={true} button={true}>
                        <ListItemText primary={`Line item ${value + 1}`}/>
                        <ListItemSecondaryAction>
                            <IconButton aria-label="Comments">
                                {!this.props.removeDisabled && <RemoveColor><i className="material-icons">remove</i></RemoveColor>}
                            </IconButton>
                        </ListItemSecondaryAction>
                    </ListItem>
                ))}
            </StyledList>
        )
    }
}

const RemoveColor = styled.span`
  color: red;
`;

export default SongList;