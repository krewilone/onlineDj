import Input from '@material-ui/core/Input';
import Slider from '@material-ui/lab/Slider';
import * as React from "react";
import styled from 'styled-components'
import {ChangeEvent} from "react";

interface IState {
    token: string;
    deviceId: string;
    volume: number;
    songName: string;
    error: string;
    player: Spotify.SpotifyPlayer | null;
}

class Player extends React.Component<{}, IState> {
    constructor(props: {}) {
        super(props);
        this.state = {
            deviceId: "",
            error: "",
            player: null,
            songName: "",
            token: "",
            volume: 0.5,
        };

        this.onTokenInserted = this.onTokenInserted.bind(this);
        this.bindPlayerEvents = this.bindPlayerEvents.bind(this);
        this.onComponentWillUnmout = this.onComponentWillUnmout.bind(this);
        this.nextSong = this.nextSong.bind(this);
        this.setVolume = this.setVolume.bind(this);
    }

    /*async componentWillMount() {
        await fetch("/test", {
            redirect: "follow"
        })
    }*/

    public onTokenInserted(event: ChangeEvent<HTMLInputElement>) {
        /*const player = new Spotify.Player({
            name: 'Web Playback SDK Quick Start Player',
            getOAuthToken: (cb: (t: string) => void) => {
                cb(value);
            }
        });


        this.bindPlayerEvents(player);*/

        const player = {
            pause: () => {console.log("pause")},
            resume: () => {console.log("resume")},
        } as Spotify.SpotifyPlayer;
        this.setState({
            player,
            token: event.currentTarget.value
        });
        // this.nextSong();
    }

    public bindPlayerEvents(player: Spotify.SpotifyPlayer) {
        // Error handling
        player.addListener('initialization_error', ({message}) => {
            console.log(message);
        });
        player.addListener('authentication_error', ({message}) => {
            console.log(message);
        });
        player.addListener('account_error', ({message}) => {
            console.log(message);
        });
        player.addListener('playback_error', ({message}) => {
            console.log(message);
        });

        // Playback status updates
        player.addListener('player_state_changed', state => {
            console.log(state);
        });

        // Ready
        player.addListener('ready', ({device_id}) => {
            this.setState({
                deviceId: device_id
            })
        });

        // Not Ready
        player.addListener('not_ready', ({device_id}) => {
            console.log('Device ID has gone offline', device_id);
        });
    }

    public onComponentWillUnmout() {
        const player = this.state.player;
        if (player) {
            player.removeListener('initialization_error');
            player.removeListener('authentication_error');
            player.removeListener('account_error');
            player.removeListener('playback_error');
            player.removeListener('player_state_changed');
            player.removeListener('ready');
            player.removeListener('not_ready');
        }
    }

    public async nextSong() {
        console.log("next song");
        /*const nextSong = await fetch("/test/next-song").then(data => data.json()).then(json => json.song);

        await fetch(`https://api.spotify.com/v1/me/player/play?device_id=${this.state.nextSong}`, {
            method: 'PUT',
            body: JSON.stringify({uris: [nextSong]}),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${this.state.token}`
            },
        });*/
    }

    public setVolume(event: any, volume: number){
        this.setState({
            volume
        });
    }

    public render() {
        if (!this.state.token) {
            return <Input placeholder="token" onChange={this.onTokenInserted}/>
        }
        console.log(this.state.player);
        return (
            <PlayerDiv>
                <InfoPanel/>
                <ColtrolPanel>
                    <ButtonDiv onClick={this.state.player!.resume}>
                        <i className="material-icons md-36">
                            play_arrow
                        </i>
                    </ButtonDiv>

                    <ButtonDiv onClick={this.state.player!.pause}>
                        <i className="material-icons md-36">
                            pause
                        </i>
                    </ButtonDiv>

                    <ButtonDiv onClick={this.nextSong}>
                        <i className="material-icons md-36">
                            fast_forward
                        </i>
                    </ButtonDiv>

                    <ButtonDiv>
                        <i className="material-icons">
                            volume_down
                        </i>
                        <div style={{width: "100px"}}><Slider value={this.state.volume} min={0} max={1} step={0.05} onChange={this.setVolume}/></div>
                        <i className="material-icons">
                            volume_up
                        </i>
                    </ButtonDiv>
                </ColtrolPanel>
            </PlayerDiv>
        )
    }
}

const ButtonDiv = styled.div`
  color: blue;
  display: flex;
  font-size: 15px;
  align-items: center;
  padding: 0 5px;
  .material-icons.md-36 { cursor: pointer }
`;

const PlayerDiv = styled.div`
  display: flex;
  flex-direction: column;
  background: darkgrey;
  border-radius: 15px;
  width: 100%;
`;

const InfoPanel = styled.div`
  display: flex;
  background: darkgrey;
  border-radius: 15px;
  width: 100%;
  height: 100%;
`;

const ColtrolPanel = styled.div`
  display: flex;
  background: darkgrey;
  border-radius: 15px;
  justify-content: center;
  width: 100%;
  height: 100%;
`;

export default Player;