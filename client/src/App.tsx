import * as React from 'react';

import Player from "./Player/Player";
import SongList from "./List/SongList";
import styled from "styled-components";

class App extends React.Component {
    public render() {
        return (
            <AppDiv>
                <SongList removeDisabled={false}/>
                <Player/>
            </AppDiv>
        );
    }
}

const AppDiv = styled.div`
  text-align: center;
  display: grid;
  grid-template-columns: 100%;
  grid-template-rows: 4fr 2fr;
  grid-row-gap: 5px
  flex-direction: column;
  height: 100%;
`;

export default App;
