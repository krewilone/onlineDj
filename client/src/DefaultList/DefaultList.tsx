import * as React from "react";
import {ListItem, ListItemText, List} from "@material-ui/core";
import styled from "styled-components";

interface IProps {
    removeDisabled: boolean;
}

interface IState {
    lists: string[];
}

class SongList extends React.Component<IProps, IState> {
    constructor(props: IProps) {
        super(props);

        this.state = {
            lists: []
        };
    }

    public async componentWillMount() {

        setTimeout(() => this.setState({
            lists: [
                "ahoj1",
                "ahoj2",
                "ahoj3",
                "ahoj4",
                "ahoj4",
                "ahoj4",
                "ahoj4",
                "ahoj4",
                "ahoj4",
                "ahoj4",
                "ahoj4",
                "ahoj4",
                "ahoj4",
                "ahoj4",
                "ahoj4",
                "ahoj4",
                "ahoj4",
                "ahoj4",
                "ahoj4",
                "ahoj4",
                "ahoj4",
                "ahoj4",
                "ahoj4",
                "ahoj4",
                "ahoj4",
                "ahoj4",
                "ahoj4",
            ]
        }));
        // const songs = await fetch("/lists").then(data => data.json()).then(json => json.lists);
        /*this.setState({
            songs
        })*/
    }

    public handleSelect = (value: string) => () => {
        console.log(`Value ${value} selected`);
    };


    public render() {
        const StyledList = styled(List)`
          overflow: auto;
        `;
        return (
            <StyledList>
                {this.state.lists.map(value => (
                    <ListItem key={value} role={undefined} dense={true} button={true} onClick={this.handleSelect(value)}>
                        <ListItemText primary={`Line item ${value + 1}`}/>
                    </ListItem>
                ))}
            </StyledList>
        )
    }
}

export default SongList;