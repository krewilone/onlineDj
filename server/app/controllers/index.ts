// Import only what we need from express
import {Router, Request, Response} from 'express';
import {createClient} from "redis";
import axios from "axios";
import {promisify} from "util";

import keys from "./keys";

const client = createClient(keys.REDIS_PORT, keys.REDIS_HOST);
const getAsync = promisify(client.get).bind(client);
const router: Router = Router();

function getRedirectUri (req: Request) {
    return `http://${req.headers.host}/test/token`;
}

router.get('/', async (req: Request, res: Response) => {
    const loggedIn = await getAsync("loggedin");
    if (!loggedIn) {
        return res.redirect(`https://accounts.spotify.com/authorize?response_type=code&client_id=${keys.SPOTIFY_TOKEN}&scope=user-read-private&redirect_uri=${encodeURIComponent(getRedirectUri(req))}`);
    }
    res.json({
        resp: "already signed in"
    });
});

router.get('/token', async (req: Request, res: Response) => {
    const loggedIn = await getAsync("loggedin");
    const redirectUri = getRedirectUri(req);
    const {code} = req.query;

    if (!loggedIn) {
        const base64 = Buffer.from(`${keys.SPOTIFY_TOKEN}:${keys.SPOTIFY_SECRET}`).toString("base64");
        try {
            const resp = await axios.post("https://accounts.spotify.com/api/token", null,{
                params: {
                    code,
                    "redirect_uri": redirectUri,
                    "grant_type": "authorization_code",
                    "client_id": keys.SPOTIFY_TOKEN,
                    "client_secret": keys.SPOTIFY_SECRET
                },
                headers: {
                    'Content-Type':'application/x-www-form-urlencoded',
                    'Authorization': `Basic ${base64}`
                }
            })
                .then(res => res.data);

            client.set("loggedin", resp.access_token);
            axios.defaults.headers.common['Authorization'] = `${resp.token_type} ${resp.access_token}`;
            res.json({
                resp: "signed in"
            });
        } catch ({message, response}) {
            res.json({
                message,
                text: response.status + response.data.error + response.data.error_description
            })
        }

    }
});

router.get("/playlists", async (_req: Request, res: Response) => {
    console.log("playlist");
    try {
        console.log(axios.defaults.headers.common['Authorization']);
        const playLists = await axios.get<SpotifyApi.ListOfCurrentUsersPlaylistsResponse>("https://api.spotify.com/v1/me/playlists").then(res => res.data.items.map(item => ({name: item.name, href: item.href})));
        console.log(playLists);
        res.json({
            playLists
        })
    } catch ({message, stack}) {
        res.json({
            message,
            stack
        })
    }
});

router.post("/choose", async (req: Request, res: Response) => {
    const selectedPlaylist: string = req.body.href;

    const songs = await axios.get<SpotifyApi.PlaylistTrackResponse>(selectedPlaylist).then(res => res.data.items.map((item) => ({uri: item.track.uri, name: item.track.name, artist: item.track.artists[0].name})));
    client.lpush("defaults", JSON.stringify(songs));
    res.json({
        songs
    })
});

router.get("/search", async (req: Request, res: Response) => {
    console.log("search", req.query.searchedTerm);
    const searchedTerm: string = req.query.searchedTerm;

    try {
        const songs = await axios.get<SpotifyApi.TrackSearchResponse>(`https://api.spotify.com/v1/search?q=${searchedTerm}&type=track&limit=10`).then(res => res.data.tracks.items);
        res.json({
            songs
        })
    } catch (e) {
        res.json(
            {error: e}
        )
    }

});

router.post("/add-tack", async (req: Request, res: Response) => {
    const selectedTrack = req.body.track;

    const song = await axios.get<SpotifyApi.TrackObjectFull>(selectedTrack).then(res => ({uri: res.data.uri, name: res.data.name, artist: res.data.artists[0].name}));
    client.rpush("custom", JSON.stringify(song));
    res.json({
        song
    })
});

router.get("/next-song", async (_req: Request, res: Response) => {
    let song = await client.lpop("custom");
    song = song ? song : client.lpop("defaults");
    res.json({
        song
    })
});

// Export the express.Router() instance to be used by server.ts
export const IndexController: Router = router;