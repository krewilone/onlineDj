const keys: Keys = {
    REDIS_HOST: process.env.REDIS_HOST || "localhost",
    REDIS_PORT: process.env.REDIS_PORT && parseInt(process.env.REDIS_PORT)|| 6379,
    SPOTIFY_SECRET: process.env.SPOTIFY_SECRET || "xx",
    SPOTIFY_TOKEN: process.env.SPOTIFY_TOKEN || "xx",
};

interface Keys {
    REDIS_HOST: string;
    REDIS_PORT: number;
    SPOTIFY_SECRET: string;
    SPOTIFY_TOKEN: string;
}

export default keys;