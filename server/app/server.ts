import * as express from 'express';
import {json, urlencoded} from 'body-parser';
import {IndexController} from './controllers';

// Create a new express application instance
const app: express.Application = express();
app.use(json());
app.use(urlencoded({
    extended: true
}));
// The port the express app will listen on
const port: number = process.env.PORT && parseInt(process.env.PORT) || 3005;

app.use('/test', IndexController);

// Serve the application at the given port
app.listen(port, () => {
    // Success callback
    console.log(`Listening at http://localhost:${port}/`);
});